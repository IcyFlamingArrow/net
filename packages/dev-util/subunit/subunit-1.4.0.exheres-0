# Copyright 2011 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# multiunpack needed for the perl part to install correctly
require launchpad [ branch=trunk ]
require perl-module python [ blacklist=none multiunpack=true ]

SUMMARY="A streaming protocol for test results"
DESCRIPTION="
Subunit is a streaming protocol for test results. The protocol is human
readable and easily generated and parsed. By design all the components of
the protocol conceptually fit into the xUnit TestCase->TestResult interaction.

Subunit comes with command line filters to process a subunit stream and
language bindings for python, C, C++ and shell. Bindings are easy to write
for other languages.
"

LICENCES="|| ( Apache-2.0 BSD-3 )"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-cpp/cppunit
        dev-libs/check[>=0.9.4]
        dev-python/extras[python_abis:*(-)?]
        dev-python/testtools[>=0.9.34][python_abis:*(-)?]
    test:
        dev-python/fixtures[python_abis:*(-)?]
        dev-python/hypothesis[python_abis:*(-)?]
        dev-python/testscenarios[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Remove-dependency-on-unittest2.patch
    "${FILES}"/${PNV}-Unittest-from-testtools-doesn-t-work-use-the-one-fro.patch
    "${FILES}"/${PNV}-Fix-imports-from-testtools.compat.patch
    "${FILES}"/${PNV}-fix-trailing-comma.patch
    "${FILES}"/${PNV}-Import-six-from-the-right-place.patch
    "${FILES}"/${PNV}-Sort-Content-Type-parameters-when-writing-details.patch
    "${FILES}"/${PNV}-Handle-different-SyntaxError-output-in-testtools-2.5.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-static )

prepare_one_multibuild() {
    default

    # Make sure we use our Perl directories
    edo sed -i -e "/@prefix@/d" perl/Makefile.PL.in
}

configure_one_multibuild() {
    default

    edo cd perl
    perl-module_src_configure
}

install_one_multibuild() {
    perl-module_src_install
    edo sed -i -e "1 s/python$/&${MULTIBUILD_TARGET}/" "${IMAGE}"/usr/$(exhost --target)/bin/*
    python_bytecompile
}

