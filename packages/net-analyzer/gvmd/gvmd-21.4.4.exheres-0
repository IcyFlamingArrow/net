# Copyright 2017-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=greenbone tag=v${PV} ] \
    cmake \
    systemd-service

SUMMARY="Greenbone Vulnerability Manager"
HOMEPAGE+=" https://www.openvas.org"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/libxslt
        virtual/pkg-config
    build+run:
        group/gvm
        user/gvm
        app-crypt/gpgme
        dev-db/postgresql:14[postgresql_extensions:pgcrypto][postgresql_extensions:uuid-ossp]
        dev-db/postgresql-client
        dev-libs/glib:2[>=2.42]
        dev-libs/gnutls[>=3.2.15]
        net-analyzer/gvm-libs[>=21.4.3]
        office-libs/libical:=[>=1.00]
    run:
        net-analyzer/openvas-scanner[>=21.4.3]
    recommendation:
        dev-perl/XML-Twig [[
            description = [ xml_split is recommended to reduce SCAP sync memory usage ]
        ]]
    suggestion:
        dev-python/gvm-tools [[
            description = [ Tools to control a GSM/GVM over GMP or OSP ]
        ]]
        dev-texlive/texlive-latex [[
            description = [ Required for exporting scan reports as PDF ]
        ]]
        dev-texlive/texlive-latexextra [[
            description = [ Required for exporting scan reports as PDF ]
        ]]
    test:
        dev-util/cppcheck
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DDATADIR:PATH=/usr/share
    -DDEBUG_FUNCTION_NAMES:BOOL=FALSE
    -DDOXYGEN_EXECUTABLE:BOOL=FALSE
    -DENABLE_COVERAGE:BOOL=FALSE
    -DGVM_RUN_DIR:PATH=/run/gvm
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DLOCALSTATEDIR:PATH=/var
    -DOPENVAS_DEFAULT_SOCKET:PATH=/run/ospd/ospd-openvas.sock
    -DSBINDIR:PATH=/usr/$(exhost --target)/bin
    -DSYSCONFDIR:PATH=/etc
    -DSYSTEMD_SERVICE_DIR:PATH=${SYSTEMDSYSTEMUNITDIR}
    -DXMLMANTOHTML_EXECUTABLE:BOOL=FALSE
    -DXMLTOMAN_EXECUTABLE:BOOL=FALSE
    -DPostgreSQL_TYPE_INCLUDE_DIR:PATH=/usr/$(exhost --target)/include/postgresql-14/server
)

src_prepare() {
    cmake_src_prepare

    # TODO: report upstream
    edo sed \
        -e 's:DESTINATION ${DATADIR}/doc/gvm:DESTINATION ${CMAKE_INSTALL_DOCDIR}:g' \
        -i CMakeLists.txt
    edo sed \
        -e 's:DESTINATION share/doc/gvm:DESTINATION ${CMAKE_INSTALL_DOCDIR}:g' \
        -i doc/CMakeLists.txt
    edo sed \
        -e 's:DESTINATION share:DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}:g' \
        -i doc/CMakeLists.txt

    # Fix build
    edo sed \
        -e 's:postgresql/libpq-fe.h:libpq-fe.h:g' \
        -i src/sql_pg.c
}

src_install() {
    cmake_src_install

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/gvm 0755 gvm gvm
f /run/gvm/gvm-checking 0600 gvm gvm
f /run/gvm/gvm-create-functions 0600 gvm gvm
f /run/gvm/gvm-helping 0600 gvm gvm
f /run/gvm/gvm-migrating 0600 gvm gvm
f /run/gvm/gvm-serving 0600 gvm gvm
EOF

    keepdir /var/{lib,log}/gvm
    keepdir /var/lib/gvm/gvmd

    edo chown gvm:gvm "${IMAGE}"/var/{lib,log}/gvm
    edo chown -R gvm:gvm "${IMAGE}"/var/lib/gvm
}

