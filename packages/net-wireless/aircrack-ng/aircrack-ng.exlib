# Copyright 2009 Maxime Coste
# Copyright 2010-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require toolchain-funcs \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_configure

SUMMARY="Tools for cracking wireless network security"
DESCRIPTION="
Aircrack-ng is an 802.11 WEP and WPA-PSK keys cracking program that can recover
keys once enough data packets have been captured. It implements the standard FMS
attack along with some optimizations like KoreK attacks, as well as the all-new
PTW attack, thus making the attack much faster compared to other WEP cracking
tools. In fact, Aircrack-ng is a set of tools for auditing wireless networks.
"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="https://download.${PN}.org/${PNV}.tar.gz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/doku.php#changelog [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/doku.php [[ lang = en ]]"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS="
    sqlite [[ description = [ Needed for airolib-ng databases ] ]]

    ( providers: gcrypt libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libpcap
        dev-libs/pcre
        net-libs/libnl:3.0[>=3.2]
        sys-apps/ethtool
        providers:gcrypt? ( dev-libs/libgcrypt[>=1.2.0] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        sqlite? ( dev-db/sqlite:3[>=3.6] )
    test:
        dev-tcl/expect
        dev-util/cmocka
"

AT_M4DIR=( build/m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-libnl
    --disable-asan
    --disable-code-coverage
    --disable-hwloc
    --disable-tsan
    --disable-valgrind
    --without-airpcap
    --without-duma
    --without-experimental
    --without-ext-scripts
    --without-gcov
    --without-jemalloc
    --without-opt
    --without-tcmalloc
    --without-xcode
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:gcrypt gcrypt'
    'sqlite sqlite3'
)

aircrack-ng_src_configure() {
    local myconf=(
        "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_WITHS[@]}" ; do \
            option_with ${s} ; \
        done )
    )

    myconf+=(
        $(cc-has-defined __AVX512__ && echo --with-avx512 || echo --without-avx512)
    )

    econf "${myconf[@]}"
}

