# Copyright (c) 2009 Bryan Østergaard
# Copyright 2010 David Vazgenovich Shakaryan <dvshakaryan@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=uzbl ]

export_exlib_phases src_prepare src_test

SUMMARY="Highly customisable minimal web browser"
DESCRIPTION="
Uzbl is a set of web interface tools which adhere to the UNIX philosophy of
writing programs that do one thing and do it well. It consists of a core
component meant for integration with other tools and scripts, as well as a
wrapper to provide a complete browsing experience based on this core.
"

HOMEPAGE="http://www.uzbl.org"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="tabbed [[ description = [ Install PyGTK wrapper for tabbed browsing ] ]]"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.19]
    build+run:
        dev-libs/glib:2
        gnome-desktop/libsoup:2.4[>=2.24]
        net-libs/webkit:3.0[>=1.6.1] [[ note = [ javascriptcoregtk-3.0 isn't included in 1.4.3 ] ]]
        x11-libs/gtk+:3
    run:
        dev-lang/python:*
        tabbed? ( gnome-bindings/pygtk )
    suggestion:
        gnome-desktop/zenity [[ description = [ Used by example scripts to display GTK+ dialog boxes ] ]]
        net-misc/socat [[ description = [ Used by example scripts to communicate with uzbl through a socket ] ]]
        x11-misc/dmenu [[ description = [ Used by example scripts to present bookmarks, history, etc. ] ]]
        x11-utils/xclip [[ description = [ Used to open pages from, and paste URIs to, the X clipboard ] ]]
"

RESTRICT="test" # requires X access

DEFAULT_SRC_COMPILE_PARAMS=( USE_GTK3=1 )
DEFAULT_SRC_INSTALL_PARAMS=( PREFIX=/usr )

uzbl_src_prepare() {
    # Tabbed wrapper is installed by default, but requires extra runtime
    # dependencies. Therefore, we should not install it unless otherwise
    # specified by the user.
    if ! option tabbed ; then
        edo sed -e '/^install:/s/ install-uzbl-tabbed//' -i Makefile
    fi
}

uzbl_src_test() {
    make tests
}

